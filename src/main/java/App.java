import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;

public class App {

    private final Calculator calculator = new CalculatorImpl();

    public void run() {
        help();
        String command = "", line;
        Scanner in = new Scanner(System.in);
        while (!command.equals(Command.EXIT)) {
            line = in.nextLine();
            String[] parts = line.split(" ");
            if (parts.length != 0) {
                executeCommand(parts);
                command = parts[0];
            }
        }
    }

    public void executeCommand(String[] parts) {
        try {
            switch (parts[0]) {
                case Command.SUM:
                    System.out.println(calculator.sum(parts[1], parts[2]));
                    break;
                case Command.FACTORIAL:
                case Command.FACTORIAL_STREAM:
                    factorial(parts);
                    break;
                case Command.FIBONACCCI:
                    System.out.println(Arrays.toString(calculator.fibonacci(parts[1])));
                    break;
            }
        } catch (IllegalArgumentException | InterruptedException | ExecutionException e) {
            System.out.println(e.getMessage());
        }
    }

    private void factorial(String[] parts) throws InterruptedException, ExecutionException {
        long current = System.currentTimeMillis();
        if (parts.length == 0 || parts.length == 1) {
            throw new IllegalArgumentException("Неверный аргументы");
        } else if (parts.length == 2) {
            if (Command.FACTORIAL.equals(parts[0])) {
                System.out.println(calculator.factorial(parts[1]));
            } else if (Command.FACTORIAL_STREAM.equals(parts[0])) {
                System.out.println(calculator.factorialStream(parts[1]));
            } else {
                System.out.println("Неверный аргументы");
            }
        } else {
            int thread = Integer.parseInt(parts[2]);
            if (thread <= 0) {
                throw new IllegalArgumentException("Количество потоков > 0 должно быть");
            }
            System.out.println(calculator.factorial(parts[1], thread).toString());
        }
        System.out.println("Time works is: " + (System.currentTimeMillis() - current) / 1000 + " s");
    }

    private void help() {
        System.out.println("Добро пожаловать, доступны команды:");
        System.out.println("sum n m");
        System.out.println("factorial n [опционально количество потоков]");
        System.out.println("factorialStream n");
        System.out.println("fibonacci n");
    }

}
