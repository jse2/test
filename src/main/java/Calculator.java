import java.math.BigInteger;
import java.util.concurrent.ExecutionException;

public interface Calculator {

    long sum(String arg1, String arg2);

    long factorial(String arg);

    BigInteger factorial(String arg, int threadsCount) throws InterruptedException, ExecutionException;

    BigInteger factorialStream(String arg);

    long[] fibonacci(String arg);

}
