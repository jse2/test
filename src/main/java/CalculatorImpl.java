import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.LongStream;

public class CalculatorImpl implements Calculator {

    public long sum(String arg1, String arg2) {
        long one = convertToLong(arg1);
        long two = convertToLong(arg2);
        return one + two;
    }

    public long factorial(String arg) {
        long one = convertToLong(arg);
        if (one < 0) {
            throw new IllegalArgumentException("Факториал сущесвует для неотрицательного целого числа");
        }
        long result = 1;
        for (long i = 1; i <= one; ++i) {
            result = safeMultiply(result, i);
        }
        return result;
    }

    public BigInteger factorial(String arg, int threadsCount) throws InterruptedException, ExecutionException {
        long max = convertToLong(arg);
        if (max < 0) {
            throw new IllegalArgumentException("Факториал сущесвует для неотрицательного целого числа");
        } else if (max == 0) {
            return BigInteger.ONE;
        }
        ExecutorService service = createExecutors(threadsCount);
        long count = max / threadsCount;
        List<CompletableFuture<BigInteger>> features = new LinkedList<>();
        for (int i = 0; i < threadsCount; ++i) {
            long start = i * count + 1;
            long finish = i == threadsCount -1 ? Math.max((i + 1) * count, max) : (i + 1) * count;
            features.add(CompletableFuture.supplyAsync(() -> factorial(BigInteger.valueOf(start), BigInteger.valueOf(finish)), service));
        }
        CompletableFuture.allOf(features.toArray(new CompletableFuture[0])).get();
        BigInteger result = BigInteger.ONE;
        for (CompletableFuture<BigInteger> feature : features) {
            result = result.multiply(feature.get());
        }
        return result;
    }

    public BigInteger factorialStream(String arg) {
        long max = convertToLong(arg);
        if (max < 0) {
            throw new IllegalArgumentException("Факториал сущесвует для неотрицательного целого числа");
        }

        return LongStream.rangeClosed(1, max)
                .mapToObj(BigInteger::valueOf)
                .parallel()
                .reduce(BigInteger.ONE, BigInteger::multiply);
    }

    private BigInteger factorial(BigInteger i, BigInteger j) {
        if (i.compareTo(j) > 0) {
            return BigInteger.ONE;
        }
        BigInteger result = BigInteger.ONE;
        while (i.compareTo(j) <= 0) {
            result = result.multiply(i);
            i = i.add(BigInteger.ONE);
        }
        return result;
    }

    public long[] fibonacci(String arg) {
        long one = convertToLong(arg);
        if (one < 0) {
            throw new IllegalArgumentException("Ряд сущесвует для неотрицательного целого числа");
        }

        long first = 0, second = 1,result = 1;
        if (one == 0) {
            return new long[] {first};
        }
        if (one == 1) {
            return new long[] {first, second};
        }
        List<Long> list = new LinkedList<>();
        list.add(first);
        list.add(second);
        for (long i = 1; i <= one; ++i) {
            long third = second + first;
            list.add(third);
            result = result + third;
            if (result == one) {
                break;
            } else if (result > one) {
                throw new IllegalArgumentException("Разложение не существуе");
            }
            first = second;
            second = third;

        }
        return list.stream().mapToLong(i -> i).toArray();
    }

    private long convertToLong(String arg) {
        try {
            return Long.parseLong(arg);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private long safeMultiply(long x, long y) {
        try {
            return Math.multiplyExact(x, y);
        } catch (ArithmeticException e) {
            throw  new IllegalArgumentException(e);
        }
    }

    private ExecutorService createExecutors(int nThreads) {
        return Executors.newFixedThreadPool(nThreads);
    }

}
