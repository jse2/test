public class Command {

    public static final String EXIT = "exit";

    public static final String SUM = "sum";

    public static final String FACTORIAL = "factorial";

    public static final String FACTORIAL_STREAM = "factorialStream";

    public static final String FIBONACCCI = "fibonacci";

}
