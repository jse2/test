import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;

import static org.junit.jupiter.api.Assertions.*;

public class CalculatorImplTest {

    private final Calculator calc = new CalculatorImpl();

    @Test
    void shouldSum() {
        assertEquals(4, calc.sum("2", "2"));
        assertEquals(0, calc.sum("2", "-2"));
        assertEquals(-1, calc.sum("-2", "1"));
    }

    @Test
    void shouldThrowSum() {
        assertThrows(IllegalArgumentException.class, () -> calc.sum("2.0", "2"));
        assertThrows(IllegalArgumentException.class, () -> calc.sum("2", "2.0"));
        assertThrows(IllegalArgumentException.class, () -> calc.sum("2,0", "2"));
        assertThrows(IllegalArgumentException.class, () -> calc.sum("2", "2,0"));
    }

    @Test
    void shouldFactorial() {
        assertEquals(1, calc.factorial("0"));
        assertEquals(120, calc.factorial("5"));
    }

    @Test
    void shouldFactorialStream() {
        assertEquals(1, calc.factorialStream("0").longValue());
        assertEquals(1, calc.factorialStream("1").longValue());
        assertEquals(120, calc.factorialStream("5").longValue());
    }

    @Test
    void shouldThrowFactorial() {
        assertThrows(IllegalArgumentException.class, () -> calc.factorial("-1"));
        assertThrows(IllegalArgumentException.class, () -> calc.factorial("1000"));
    }

    @Test
    void shouldFibonacci() {
        assertTrue(Arrays.equals(new long[] { 0 }, calc.fibonacci("0")));
        assertTrue(Arrays.equals(new long[] { 0, 1 }, calc.fibonacci("1")));
        assertTrue(Arrays.equals(new long[] { 0, 1, 1, 2, 3, 5, 8, 13, 21 }, calc.fibonacci("54")));
    }

    @Test
    void shouldThrowFibonacci() {
        assertThrows(IllegalArgumentException.class, () -> calc.fibonacci("-1"));
        assertThrows(IllegalArgumentException.class, () -> calc.fibonacci("53"));
    }

    @Test
    void shouldEqualFactorial() throws InterruptedException, ExecutionException {
        assertEquals(calc.factorial("5"), calc.factorial("5", 1).longValue());
        assertEquals(calc.factorial("20"), calc.factorial("20", 1).longValue());

        assertEquals(calc.factorial("5"), calc.factorial("5", 4).longValue());
        assertEquals(calc.factorial("20"), calc.factorial("20", 4).longValue());

        assertEquals(calc.factorial("5"), calc.factorialStream("5").longValue());
        assertEquals(calc.factorial("20"), calc.factorialStream("20").longValue());

        assertEquals(calc.factorialStream("10000"), calc.factorial("10000", 10));
        assertEquals(calc.factorialStream("1000000"), calc.factorial("1000000", 10));
    }


    @Test
    void shouldParallelFactorialMoreFasterThanSequantial() throws InterruptedException, ExecutionException {
        long current = System.currentTimeMillis();
        BigInteger sequantialFactorial = calc.factorial("100000", 1);
        long sequantial = System.currentTimeMillis() - current;

        current = System.currentTimeMillis();
        BigInteger parallelFactorial = calc.factorial("100000", 10);
        long parallel = System.currentTimeMillis() - current;

        assertTrue(sequantialFactorial.equals(parallelFactorial));
        assertTrue(parallel < sequantial);
    }

    @Test
    void shouldParallelStreamFactorialMoreFasterThanSequantial() throws InterruptedException, ExecutionException {
        long current = System.currentTimeMillis();
        BigInteger sequantialFactorial = calc.factorial("100000", 1);
        long sequantial = System.currentTimeMillis() - current;

        current = System.currentTimeMillis();
        BigInteger parallelFactorial = calc.factorialStream("100000");
        long parallel = System.currentTimeMillis() - current;

        assertTrue(sequantialFactorial.equals(parallelFactorial));
        assertTrue(parallel < sequantial);
    }

}
